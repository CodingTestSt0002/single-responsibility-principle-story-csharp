namespace SingleResponsibilityPrincipleStory.Models
{
    public class Contact
    {
        public enum ContactType
        {
            EMAIL,
            SKYPE,
            FACEBOOK,
            LINKEDIN
        }

        public ContactType Type { get; set; }

        public string ContactData { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as Contact;

            if (item == null)
            {
                return false;
            }

            return Type == item.Type && ContactData.Equals(item.ContactData);
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode() ^ ContactData.GetHashCode();
        }
    }
}
