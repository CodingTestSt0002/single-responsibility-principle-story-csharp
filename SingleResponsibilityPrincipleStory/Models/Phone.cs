namespace SingleResponsibilityPrincipleStory.Models
{
    public class Phone
    {
        public enum PhoneType
        {
            PRIMARY,
            HOME,
            WORK,
            MOBILE
        }

        public PhoneType Type { get; set; }

        public string Number { get; set; }
      
    }
}