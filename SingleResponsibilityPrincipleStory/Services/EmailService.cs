﻿using System.Linq;
using System.Net.Mail;
using HandlebarsDotNet;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SingleResponsibilityPrincipleStory.Models;
using SingleResponsibilityPrincipleStory.Resources;

namespace SingleResponsibilityPrincipleStory.Services
{
    public class EmailService
    {
        private const string OwnerName = "John Doe";
        private const string OwnerEmail = "jdoe@gmail.com";
        private const string EmailSubject = "Your email contact was added to contacts";

        private readonly SmtpConfig smtpConfig;
        private readonly ILogger<EmailService> logger;

        public EmailService(IOptions<SmtpConfig> smtpConfig,
            ILogger<EmailService> logger)
        {
            this.smtpConfig = smtpConfig.Value;
            this.logger = logger;
        }

        public void GreetPerson(Person person)
        {
            var recipient = person.Contacts.FirstOrDefault(c => c.Type == Contact.ContactType.EMAIL)?.ContactData;

            try
            {
                string body = PrepareEmailBody(person, PersonController_Templates.GreetingEmailTemplate);
                SendEmail(recipient, OwnerEmail, EmailSubject, body);
                logger.LogInformation($"Greeting email was sent to {recipient}");
            }
            catch (SmtpException e)
            {
                logger.Log(LogLevel.Error, e, "Email sending error.");
            }
        }

        private void SendEmail(string from, string to, string subject, string body)
        {
            var mail = new MailMessage(from, to, subject, body);
            var client = new SmtpClient
            {
                Port = smtpConfig.Port,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = smtpConfig.Server,
                Credentials = new System.Net.NetworkCredential(smtpConfig.User, smtpConfig.Pass)
            };
            client.Send(mail);
        }

        private string PrepareEmailBody(Person person, string emailTemplate)
        {
            var template = Handlebars.Compile(emailTemplate);
            return template(new { person.FirstName, person.LastName, OwnerName });
        }
    }
}
