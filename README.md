# Single Responsibility Principle Refactoring Story
**To read**: [<link to story opened in Refactories Story UI, just link from browser is good enough>]

**Estimated reading time**: <estimate, change later after feedback>

## Story Outline
Welcome to refactoring story about Single Responsibility Principle (SRP) for SOLID training course.
This story is about proper decomposition of classes and methods to fit Single Responsibility Principle.
In this story you will see example of web service application that is able to store and manage information
about persons and their contacts. Also you will see application evolution during refactoring 
and improvements steps.

### Contacts Book
Contacts Book is a REST web service which exposes personal data as JSON.
Project is built using ASP.NET Core, so it utilizes simplicity of
coding, deployment, and data exposing using .NET Core 2.1. 

Class [Person](/SingleResponsibilityPrincipleStory/Models/Person.cs) keeps persons 
information about name, birthday, living address and various contacts like phone and IM.
[Person](/SingleResponsibilityPrincipleStory/Models/Person.cs) is exposed as REST resource 
to store adn query by personal data using person's email as a primary key.

##### Run application
Solution [SingleResponsibilityPrincipleStory](/SingleResponsibilityPrincipleStory.sln) can be opened in Visual Studio and can be started by pressing Ctrl+F5 or by going to the Debug menu. Visual Studio uses IIS Express to host your application.

##### Store new contact
As you see application is listening on port 50596. Host setting could be changed in [launchSettings](/SingleResponsibilityPrincipleStory/Properties/launchSettings.json)
To add new contact you should POST request to:
```
http://localhost:50596/person/contact
```
With the following JSON in the body
```
{
    "firstName": "John",
    "lastName": "Doe",
    "birthday": "2000-01-01",
    "contacts" : [{ "type":"EMAIL", "contactdata":"jdoe@email.dog"}]
}
```

##### Query contact by email
You might query contact by issuing GET request like:
```
http://localhost:50596/person/contact?email=jdoe@email.dog
```
Which might response with following JSON:
```
{
    "firstName": "John",
    "lastName": "Doe",
    "birthday": "2000-01-01T00:00:00",
    "addresses": null,
    "phones": null,
    "contacts": [
        {
            "type": 0,
            "contactData": "jdoe@email.dog"
        }
    ]
}
```
##### Application evolution and extensions planning
At the first glance application is able to fulfill it's goals. But it might be not well
suitable for further evolution and extensions because of many violations.
One of most annoying here is Single Responsibility Principle violation.
Improving only that aspect alone we might dramatically improve application structure
and "fix" other violations.